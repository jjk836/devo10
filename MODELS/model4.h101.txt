CH1	AIL	Dual Rate on AIL DR
CH2	ELE	Dual Rate on ELE DR
CH3	THR	throttle curve on MIX 0=1-1, 1=heavy model, 2=light model
CH4	RUD	Dual Rate on RUD DR
CH5	TRIM_L+ invert (virt7, virt8, trim7) push button and move cyclic to direction
CH6	TRIM_R+	flip (virt5, virt6, trim6) push button and move cyclic to direction
CH9	FMODE 	Flightmode (0=head mode, 1=headless mode) 
CH10	FMODE2  Return to home (not enough virtchans for auto off in the moment)
	TRIM_R- Set new heading (only if THR=0, virt3)
	TRIM_L- Gyro callibration (only if THR=0, virt9)
	GEAR	Emergency throttle hold (virt4)

virt1	abscyclic
virt2	absthr
virt10  flip or invert mode active

